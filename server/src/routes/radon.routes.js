const express = require('express');
// var request = require('request');
const router = express.Router();
const radonController = require('./../controller/radonController')

router.get('/radon',radonController.devuelveRadon);

module.exports = router;
