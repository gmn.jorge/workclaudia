var request = require('request');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express();
const cors = require('cors');
const task = require('./routes/task.routes');
const radonrouter = require('./routes/radon.routes');

const bodyParser = require("body-parser");

var urlencodedParser = bodyParser.urlencoded({ extended: false });

//const { mongoose } = require('./database');

//Settings
app.set('port', process.env.PORT || 3000); //nombre de la variable del puerto y el puerto asignado.

app.use(
    bodyParser.urlencoded({
      limit: "10mb",
      parameterLimit: 100000,
      extended: true, //true
    })
  );

//create application/json parser
app.use(
    bodyParser.json({
      limit: "10mb",
    })
  );

app.use(cors())

//Middlewares
app.use(morgan('dev'));
app.use(express.json());//comprueba si las funciones pasadas están en formato json.

//Routes
// app.use('/api/tasks', task);
app.use('/api', radonrouter);
// app.use('/api/tasks', require(req, rest));

//Static files
app.use(express.static(path.join(__dirname, 'public')));//concatena la ruta para llegar a la carpeta public y leer los html

//server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});

