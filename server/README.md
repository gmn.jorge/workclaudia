# Grupo 50

Este es el repositorio del Grupo 50, cuyos integrantes son:

* Mauricio Cortés   - 202004529-1
* Gianfranco Dissi  - 202004514-3
* Claudia Guzmán    - 202004546-1
* **Tutor**: Alan Nuñez

## Wiki

Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.inf.utfsm.cl/alnunez/inf-236-2022-2-grupo-50/-/wikis/home)

## Videos

* [Video presentación cliente   -   Password: UTFSM](https://vimeo.com/746676729)
* [Video presentacion avance 1  -   Password: utfsm](https://vimeo.com/749821099)
* [Video presentación final](*pendiente*)

