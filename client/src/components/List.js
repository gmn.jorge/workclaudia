import { Grid } from '@material-ui/core';
import React, {Fragment} from 'react'
import PokeCard from './PokeCard';

/* const List = (props)=>{
    return(<h1>Lista de Pokemons</h1>)
} */

function List ({pokeData}){
    return(
        <Fragment>
            <h1>Lista de Pokemons</h1>
                <Grid container spacing={24} justify= 'center'>
                {pokeData.map((pokemon, index) =>{
                    let url= 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
                    let pokeIndex = pokemon.url.split('/')[pokemon.url.split('/').length - 2];
                    console.log(pokeIndex)
                    console.log(`${url}${pokeIndex}.png`)
                    return (
                        <PokeCard name={pokemon.name} image={`${url}${pokeIndex}.png`}/>)
                    })}
                </Grid>
            
        </Fragment>
    );
}

export default List;