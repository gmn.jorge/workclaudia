import { Grid } from '@material-ui/core';
import React, {Fragment} from 'react'
import PokeCard from './PokeCard';

/* const List = (props)=>{
    return(<h1>Lista de Pokemons</h1>)
} */

function ListaDeRadon ({pokeData}){
    return(
        <Fragment>
            <h1>Lista de Radon</h1>
                <Grid container spacing={24} justify= 'center'>
                {pokeData.map((pokemon, index) =>{
                    return (
                        <PokeCard name={pokemon.name} />)
                    })}
                </Grid>
            
        </Fragment>
    );
}

export default ListaDeRadon;