import React from "react";
import {AppBar, Typography,Toolbar} from '@material-ui/core'
import { withStyles } from "@material-ui/core";

function AppNav(props){

    const {classes}=props;
    return(
        <AppBar className= {classes.NavColor} position='static'>
            <Toolbar variant='dense'>
                <Typography variant='h5' component='p'>Poke App</Typography>
            </Toolbar>
        </AppBar>
    );
}

export default withStyles({NavColor:{backgroundColor:'#6633FF'}})(AppNav)