import React from 'react'
import Home from './components/Home';
import {Switch, Route} from "react-router-dom";
import PokeListContainer from './containers/PokeListContainer';
import RadonListContainer from './containers/RadonListContainer';

const Routes = ()=>{
    return(
        <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route path="/pokemons" component= {PokeListContainer}></Route>
            <Route path="/radon" component= {RadonListContainer}></Route>
        </Switch>
    );
}

export default Routes;